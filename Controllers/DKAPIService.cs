﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;


namespace DKWebAPI
{


    [Route("api/[controller]")]
    [ApiController]

    public class DKAPIService : ControllerBase
    {

        private readonly IDKAPIRepository _dkAPIRepo;

        public DKAPIService(IDKAPIRepository dkAPIRepo)
        {
            _dkAPIRepo  = dkAPIRepo;            
        }


        #region GET ROUTINES

        /// <summary>
        /// Check on OAUTH 2.0 Tokens
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "GetTokens")]
        public async Task<ActionResult<ApiClientSettings>> GetTokens()
        {
            try
            {
                new DKAPIService(_dkAPIRepo);
                // Get API client seetings (tokens)
                return await _dkAPIRepo.GetTokens();
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// Calls Products API and stores result in SQL
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "Products/{searchTerm}")]
        public async Task<string> Products(string searchTerm)
        {
            try
            {
                new DKAPIService(_dkAPIRepo);

                return await _dkAPIRepo.Products(searchTerm);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// Calls OrderStatus API 
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "OrderStatus/{salesOrderId}")]
        public async Task<string> OrderStatus(int salesOrderId)
        {
            try
            {
                new DKAPIService(_dkAPIRepo);

                return await _dkAPIRepo.OrderStatus(salesOrderId); 
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /// <summary>
        /// Calls OrderHistory API 
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "OrderHistory/{customerID}")]
        public async Task<string> OrderHistory(int customerID)
        {
            try
            {
                new DKAPIService(_dkAPIRepo);

                return await _dkAPIRepo.OrderHistory(customerID);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }


        /// <summary>
        /// Calls Products API with option to store result in SQL
        /// </summary>  
        [HttpGet]
        [EnableCors("CorsPolicy")]
        [Route(template: "Products/{searchTerm}/{writeToSQL}")]
        public async Task<string> Products(string searchTerm, bool writeToSQL)
        {
            try
            {
                new DKAPIService(_dkAPIRepo);

                return await _dkAPIRepo.Products(searchTerm, writeToSQL);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        #endregion

        #region POST ROUTINES

        /// <summary>
        /// Calls Keyword Search API Using POST Verb
        /// </summary>  
        [HttpPost]
        [EnableCors("CorsPolicy")]
        [Route(template: "KeywordSearch")]
        public async Task<string> KeywordSearch(KeywordSearchRequest searchTerm)
        {
            try
            {
                new DKAPIService(_dkAPIRepo);

                return await _dkAPIRepo.KeywordSearch(searchTerm);
            }
            catch (System.NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        #endregion
    
    }


    }
