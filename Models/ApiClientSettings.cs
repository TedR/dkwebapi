using System;

namespace DKWebAPI {


    public class ApiClientSettings
    {
        public String ClientId { get; set; }
        public String ClientSecret { get; set; }
        public String RedirectUri { get; set; }
        public String AccessToken { get; set; }
        public String RefreshToken { get; set; }
        public DateTime ExpirationDateTime { get; set; }

    }

}