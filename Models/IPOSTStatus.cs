using System;


namespace DKWebAPI 

{
    public interface IPOSTStatus
    {
        int StatusCode { get; set; }
        string StatusMessage { get; set; }
    }

}