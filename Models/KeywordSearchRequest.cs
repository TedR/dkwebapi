using System;
using System.Collections.Generic;

namespace DKWebAPI {


    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ParametricFilter
    {
        public int ParameterId { get; set; }
        public string ValueId { get; set; }
    }

    public class Filters
    {
        public List<int> TaxonomyIds { get; set; }
        public List<int> ManufacturerIds { get; set; }
        public List<ParametricFilter> ParametricFilters { get; set; }
    }

    public class Sort
    {
        public string SortOption { get; set; }
        public string Direction { get; set; }
        public int SortParameterId { get; set; }
    }

    public class KeywordSearchRequest
    {
        public string Keywords { get; set; }
        public int RecordCount { get; set; }
        public int RecordStartPosition { get; set; }
        public Filters Filters { get; set; }
        public Sort Sort { get; set; }
        public int RequestedQuantity { get; set; }
        public List<string> SearchOptions { get; set; }
        public bool ExcludeMarketPlaceProducts { get; set; }
    }




}

