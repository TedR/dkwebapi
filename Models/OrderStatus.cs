using System;


namespace DKWebAPI


{
    public class OrderStatus
    {

        public OrderStatus()
        {
        }

        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }

    }

}