using System;


namespace DKWebAPI


{
    public class POSTStatus : IPOSTStatus
    {

        public POSTStatus()
        {
        }

        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }

    }

}