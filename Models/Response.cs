using System.Collections.Generic;

namespace DKWebAPI
{

    public class StandardPricingItem
{
    /// <summary>
    /// 
    /// </summary>
    public int BreakQuantity { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public double UnitPrice { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public double TotalPrice { get; set; }
}

public class ParametersItem
{
    /// <summary>
    /// 
    /// </summary>
    public int ParameterId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ValueId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Parameter { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Value { get; set; }
}

public class Series
{
    /// <summary>
    /// 
    /// </summary>
    public int ParameterId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ValueId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Parameter { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Value { get; set; }
}

public class Packaging
{
    /// <summary>
    /// 
    /// </summary>
    public int ParameterId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ValueId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Parameter { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Value { get; set; }
}

public class Manufacturer
{
    /// <summary>
    /// 
    /// </summary>
    public int ParameterId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ValueId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Parameter { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Value { get; set; }
}

public class ProductsItem
{
    /// <summary>
    /// 
    /// </summary>
    public List<StandardPricingItem> StandardPricing { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string RoHSStatus { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string LeadStatus { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public List<ParametersItem> Parameters { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ProductUrl { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string PrimaryDatasheet { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string PrimaryPhoto { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string PrimaryVideo { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public Series Series { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ManufacturerLeadWeeks { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ManufacturerPageUrl { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ProductStatus { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public List<string> AlternatePackaging { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string DetailedDescription { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string TariffDescription { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ManufacturerPartNumber { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int MinimumOrderQuantity { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string NonStock { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public Packaging Packaging { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int QuantityAvailable { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string DigiKeyPartNumber { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ProductDescription { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int UnitPrice { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public Manufacturer Manufacturer { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int ManufacturerPublicQuantity { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int QuantityOnOrder { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string DKPlusRestriction { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string SupplierDirectShip { get; set; }
}

public class Root
{
    /// <summary>
    /// 
    /// </summary>
    public List<ProductsItem> Products { get; set; }
}

}