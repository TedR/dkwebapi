using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json; 
using Dapper;
using System.Data;
using System.Data.SqlClient;


namespace DKWebAPI
{

    public class ApiClientService
    {

        private readonly IConfiguration _config;

        private ApiClientSettings _clientSettings;
    
        public ApiClientSettings ClientSettings
        {
            get => _clientSettings;
            set => _clientSettings = value;
        }
        public IDbConnection Connection
        {
            get
            {

                return new SqlConnection(_config.GetConnectionString("EFModel"));

            }
        }


        /// <summary>
        ///     The httpclient which will be used for the api calls through the this instance
        /// </summary>
        public HttpClient HttpClient { get; private set; }

        public ApiClientService(ApiClientSettings clientSettings, IConfiguration config)
        {
            ClientSettings = clientSettings ?? throw new ArgumentNullException(nameof(clientSettings));
            _config = config ?? throw new ArgumentNullException(nameof(config));
            Initialize();
        }

        private void Initialize()
        {
            HttpClient = new HttpClient();

            System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls12;

            var authenticationHeaderValue = new AuthenticationHeaderValue("Bearer", ClientSettings.AccessToken);
            HttpClient.DefaultRequestHeaders.Authorization = authenticationHeaderValue;

            HttpClient.DefaultRequestHeaders.Add("X-Digikey-Client-Id", ClientSettings.ClientId);
            HttpClient.DefaultRequestHeaders.Add("X-Digikey-Locale-Site", "UK");
            HttpClient.DefaultRequestHeaders.Add("X-Digikey-Locale-Language", "en");
            HttpClient.DefaultRequestHeaders.Add("X-Digikey-Locale-Currency", "GBP");
            HttpClient.DefaultRequestHeaders.Add("X-Digikey-Locale-ShipToCountry", "GB");
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));            
            HttpClient.BaseAddress = new Uri(_config.GetSection("Endpoint").GetValue("BaseURL", ""));

        }


        public async Task<string> GetNewToken(string token)
        {
            var resourcePath = "/v1/oauth2/token/";

            var postResponse = await PostAsJsonAsync(resourcePath, _clientSettings);

            // var jsonObject = JsonConvert.DeserializeObject<Root>(GetServiceResponse(postResponse).Result);

            return GetServiceResponse(postResponse).Result;
        }

        public async Task<string> KeywordSearch(KeywordSearchRequest keywordSearch)
        {
            var resourcePath = "/Search/v3/Products/Keyword";

            var request = keywordSearch; 

    
            var postResponse = await PostAsJsonAsync(resourcePath, request);

            // This will be stored in SQL
            // var jsonObject = JsonConvert.DeserializeObject<Root>(GetServiceResponse(postResponse).Result);

            return GetServiceResponse(postResponse).Result;
        }

        public async Task<string> Products(string searchTerm)
        {
            var resourcePath = "/Search/v3/Products/" + searchTerm;

            var getResponse = await HttpClient.GetAsync(resourcePath);
            
            // This will be stored in SQL
            // var jsonObject = JsonConvert.DeserializeObject<Root>(GetServiceResponse(getResponse).Result);

            var responseString = GetServiceResponse(getResponse).Result;

            var outcome = await WriteToSQL(responseString);

            return responseString;
        }

        public async Task<string> Products(string searchTerm, bool writeToSQL)
        {
            var resourcePath = "/Search/v3/Products/" + searchTerm;

            var getResponse = await HttpClient.GetAsync(resourcePath);

            // This will be stored in SQL
            // var jsonObject = JsonConvert.DeserializeObject<Root>(GetServiceResponse(getResponse).Result);

            var responseString = GetServiceResponse(getResponse).Result;

            if (writeToSQL ) {
                 var outcome = await WriteToSQL(responseString);
            }

            return responseString;
        }

        protected async Task<POSTStatus> WriteToSQL(string response)
        {
            // Get JSON Tokens to access Model
            JObject jo = JObject.Parse(response);
            // Create PARAMS
            var p = new DynamicParameters();
                       
            /*
            foreach (var item in jo) 
            {

                Console.WriteLine(item.Key + " : " + item.Value + " :: " + item.Value.GetType().ToString() + " ::: " + item.Value.HasValues);            

                if (item.Value.Type == JTokenType.Boolean) {
                    p.Add("@" + item.Key, item.Value == null  ? false : item.Value);
                } else if (item.Value.Type == JTokenType.String) {
                    p.Add("@" + item.Key, item.Value == null ? "n/a" : item.Value);
                } else if (item.Value.Type == JTokenType.Integer) {
                    p.Add("@" + item.Key, item.Value == null ? 0 : item.Value);
                } else {
                    p.Add("@" + item.Key, item.Value);
                }

            }
            */

            #region Params
            
            JToken node = jo.SelectToken("MyPricing");
            p.Add("@MyPricing", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("Obsolete");
            p.Add("@Obsolete", node == null  ? false : jo.Value<Boolean>("Obsolete"), DbType.Boolean);

            node = jo.SelectToken("MediaLinks");
            p.Add("@MediaLinks", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("StandardPackage");
            p.Add("@StandardPackage", node == null ? 0 : jo.Value<Int32>("StandardPackage"), DbType.Int32);

            node = jo.SelectToken("LimitedTaxonomy");
            p.Add("@LimitedTaxonomy", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("Kits");
            p.Add("@Kits", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("KitContents");
            p.Add("@KitContents", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("MatingProducts");
            p.Add("@MatingProducts", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("SearchLocaleUsed");
            p.Add("@SearchLocaleUsed", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("AssociatedProducts");
            p.Add("@AssociatedProducts", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ForUseWithProducts");
            p.Add("@ForUseWithProducts", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("RohsSubs");
            p.Add("@RohsSubs", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("SuggestedSubs");
            p.Add("@SuggestedSubs", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("AdditionalValueFee");
            p.Add("@AdditionalValueFee", node == null ? 0 : jo.Value<Double>("AdditionalValueFee"), DbType.Double);

            node = jo.SelectToken("ReachEffectiveDate");
            p.Add("@ReachEffectiveDate", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("StandardPricing");
            p.Add("@StandardPricing", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("RoHSStatus");
            p.Add("@RoHSStatus", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("LeadStatus");
            p.Add("@LeadStatus", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("Parameters");
            p.Add("@Parameters", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ProductUrl");
            p.Add("@ProductUrl", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("PrimaryDatasheet");
            p.Add("@PrimaryDatasheet", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("PrimaryPhoto");
            p.Add("@PrimaryPhoto", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("PrimaryVideo");
            p.Add("@PrimaryVideo", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("Series");
            p.Add("@Series", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ManufacturerLeadWeeks");
            p.Add("@ManufacturerLeadWeeks", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ManufacturerPageUrl");
            p.Add("@ManufacturerPageUrl", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ProductStatus");
            p.Add("@ProductStatus", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("DateLastBuyChance");
            p.Add("@DateLastBuyChance", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("AlternatePackaging");
            p.Add("@AlternatePackaging", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("DetailedDescription");
            p.Add("@DetailedDescription", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ReachStatus");
            p.Add("@ReachStatus", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ExportControlClassNumber");
            p.Add("@ExportControlClassNumber", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("HTSUSCode");
            p.Add("@HTSUSCode", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("TariffDescription");
            p.Add("@TariffDescription", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ManufacturerPartNumber");
            p.Add("@ManufacturerPartNumber", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("MinimumOrderQuantity");
            p.Add("@MinimumOrderQuantity", node == null ? 0 : jo.Value<Int32>("StandardPackage"), DbType.Int32);

            node = jo.SelectToken("NonStock");
            p.Add("@NonStock", node == null ? false : jo.Value<Boolean>("NonStock"), DbType.Boolean);

            node = jo.SelectToken("Packaging");
            p.Add("@Packaging", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("QuantityAvailable");
            p.Add("@QuantityAvailable", node == null ? 0 : jo.Value<Int32>("StandardPackage"), DbType.Int32);

            node = jo.SelectToken("DigiKeyPartNumber");
            p.Add("@DigiKeyPartNumber", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ProductDescription");
            p.Add("@ProductDescription", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("UnitPrice");
            p.Add("@UnitPrice", node == null ? 0 : jo.Value<Double>("UnitPrice"), DbType.Double);

            node = jo.SelectToken("Manufacturer");
            p.Add("@Manufacturer", node == null ? "n/a" : node.ToString(), DbType.String);

            node = jo.SelectToken("ManufacturerPublicQuantity");
            p.Add("@ManufacturerPublicQuantity", node == null ? 0 : jo.Value<Int32>("ManufacturerPublicQuantity"), DbType.Int32);

            node = jo.SelectToken("QuantityOnOrder");
            p.Add("@QuantityOnOrder", node == null ? 0 : jo.Value<Int32>("QuantityOnOrder"), DbType.Int32);

            node = jo.SelectToken("StandardPackage");
            p.Add("@StandardPackage", node == null ? 0 : jo.Value<Int32>("StandardPackage"), DbType.Int32);

            node = jo.SelectToken("DKPlusRestriction");
            p.Add("@DKPlusRestriction", node == null ? false : jo.Value<Boolean>("DKPlusRestriction"), DbType.Boolean);

            node = jo.SelectToken("SupplierDirectShip");
            p.Add("@SupplierDirectShip", node == null ? false : jo.Value<Boolean>("SupplierDirectShip"), DbType.Boolean);

            #endregion


            string sql = 
                @"INSERT INTO DKProduct_Details(MyPricing, Obsolete, MediaLinks, StandardPackage, LimitedTaxonomy,
                Kits, KitContents, MatingProducts, SearchLocaleUsed, AssociatedProducts, ForUseWithProducts, RohsSubs,
                SuggestedSubs, AdditionalValueFee, ReachEffectiveDate, StandardPricing, RoHSStatus, LeadStatus, Parameters,
                ProductUrl, PrimaryDatasheet, PrimaryPhoto, PrimaryVideo, Series, ManufacturerLeadWeeks, ManufacturerPageUrl,
                ProductStatus, DateLastBuyChance, AlternatePackaging, DetailedDescription, ReachStatus, ExportControlClassNumber,
                HTSUSCode, TariffDescription, ManufacturerPartNumber, MinimumOrderQuantity, NonStock, Packaging, QuantityAvailable,
                DigiKeyPartNumber, ProductDescription, UnitPrice, Manufacturer, ManufacturerPublicQuantity, QuantityOnOrder, 
                DKPlusRestriction,  SupplierDirectShip, AddedBySystem) 
                VALUES(@MyPricing, @Obsolete, @MediaLinks, @StandardPackage, @LimitedTaxonomy,
                @Kits, @KitContents, @MatingProducts, @SearchLocaleUsed, @AssociatedProducts, @ForUseWithProducts, @RohsSubs,
                @SuggestedSubs, @AdditionalValueFee, @ReachEffectiveDate, @StandardPricing, @RoHSStatus, @LeadStatus, @Parameters,
                @ProductUrl, @PrimaryDatasheet, @PrimaryPhoto, @PrimaryVideo, @Series, @ManufacturerLeadWeeks, @ManufacturerPageUrl,
                @ProductStatus, @DateLastBuyChance, @AlternatePackaging, @DetailedDescription, @ReachStatus, @ExportControlClassNumber,
                @HTSUSCode, @TariffDescription, @ManufacturerPartNumber, @MinimumOrderQuantity, @NonStock, @Packaging, @QuantityAvailable,
                @DigiKeyPartNumber, @ProductDescription, @UnitPrice, @Manufacturer, @ManufacturerPublicQuantity, @QuantityOnOrder, 
                @DKPlusRestriction,  @SupplierDirectShip, CURRENT_TIMESTAMP);";
            
            try {            
                using (IDbConnection conn = Connection)
                {
                    var affectedRows = await conn.ExecuteAsync(sql, p, commandType: CommandType.Text);
                    // Check if worked ..
                    if (affectedRows > 0)
                    {
                        return await Task.Run(() => new POSTStatus
                        { StatusCode = 1, StatusMessage = String.Format("Status: OK, Added: {0} {1}", affectedRows, "Row") });
                    }
                }

                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.Message);
                    // Arrived here as update failed so return an error object
                    return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Adding Data" });
                }
            // Arrived here as update failed so return an error object
            return await Task.Run(() => new POSTStatus { StatusCode = -1, StatusMessage = "Error Data" });
        }

        protected async Task<HttpResponseMessage> PostAsJsonAsync<T>(string resourcePath, T postRequest)
        {
            
            try
            {
                var response = await HttpClient.PostAsJsonAsync(resourcePath, postRequest);
                

                //Unauthorized, then there is a chance token is stale
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var responseBody = await response.Content.ReadAsStringAsync();
                }

                return response;
            }
            catch (HttpRequestException)
            {
                throw;
            }

        }

        protected async Task<string> GetServiceResponse(HttpResponseMessage response)
        {
            var postResponse = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                if (response.Content != null)
                {
                    postResponse = await response.Content.ReadAsStringAsync();
                    response.Content = new StringContent(postResponse, System.Text.Encoding.UTF8, "application/json");
                }
            }
            else
            {
                var errorMessage = await response.Content.ReadAsStringAsync();
                Console.WriteLine("Response");
                Console.WriteLine("  Status Code : {0}", response.StatusCode);
                Console.WriteLine("  Content     : {0}", errorMessage);
                Console.WriteLine("  Reason      : {0}", response.ReasonPhrase);
                var resp = new HttpResponseMessage(response.StatusCode)
                {
                    Content = response.Content,                    
                    ReasonPhrase = response.ReasonPhrase
                };
                throw new HttpResponseException(resp);
            }


            return postResponse;
        }


        public async Task<string> OrderStatus(int salesOrderId)
        {
            var resourcePath = "/OrderDetails/v3/Status/" + salesOrderId.ToString() ;

            var getResponse = await HttpClient.GetAsync(resourcePath);


            //var jsonObject = JsonConvert.DeserializeObject<Root>(GetServiceResponse(getResponse).Result);

            var responseString = GetServiceResponse(getResponse).Result;

            return responseString;
        }


        public async Task<string> OrderHistory(int customerID)
        {
            var resourcePath = "/OrderDetails/v3/History?CustomerID=" + customerID.ToString() + "&OpenOnly=true&IncludeCompanyOrders=true";

            var getResponse = await HttpClient.GetAsync(resourcePath);


            var responseString = GetServiceResponse(getResponse).Result;

            return responseString;
        }

    }
           

}