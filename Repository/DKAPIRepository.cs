using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc;

namespace DKWebAPI
{


    public class DKAPIRepository : ControllerBase, IDKAPIRepository {


        #region Definitions and Variables
        private readonly IConfiguration _config;

        private ApiClientSettings _clientSettings;

        public ApiClientSettings ClientSettings
        {
            get => _clientSettings;
            set => _clientSettings = value;
        }

        public DKAPIRepository(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection Connection
        {
            get
            {

                return new SqlConnection(_config.GetConnectionString("EFModel"));

            }
        }

        #endregion

        #region Methods

        async Task<ApiClientSettings> IDKAPIRepository.GetRefreshToken(string token)
        {
            // This needs to call DK API Directly
            await this.Settings();
            // Override config url to token endpoint
            var api = new ApiClientService(ClientSettings, _config);
            var result = await api.GetNewToken(token);
            return ClientSettings;
        }

        async Task<ApiClientSettings> IDKAPIRepository.GetTokens()
        {
            await this.Settings();
            return ClientSettings;
        }

        // Invoke Keyword Search as internal POST
        async Task<string> IDKAPIRepository.KeywordSearch(KeywordSearchRequest searchTerm)
        {
            await this.Settings();
            var api = new ApiClientService(ClientSettings, _config);

            var result = await api.KeywordSearch(searchTerm);

            return result;

        }

        // Invoke DK Products -> Search using GET Method
        async Task<string> IDKAPIRepository.Products(string searchTerm) {
            await this.Settings();
            var api = new ApiClientService(ClientSettings, _config);
            var result = await api.Products(searchTerm);
            return result;
        }


        // Invoke DK Products -> Search using GET Method
        // Option of SQL Storage for Quotation System
        async Task<string> IDKAPIRepository.Products(string searchTerm, bool writeToSQL)
        {
            await this.Settings();
            var api = new ApiClientService(ClientSettings, _config);
            var result = await api.Products(searchTerm, writeToSQL);
            return result;
        }



        // Invoke DK -> Search using GET Method
        async Task<string> IDKAPIRepository.OrderStatus(int salesOrderID) 
        {
            await this.Settings();
            var api = new ApiClientService(ClientSettings, _config);
            var result = await api.OrderStatus(salesOrderID);
            return result;
        }

        // Invoke DK -> Search using GET Method
        async Task<string> IDKAPIRepository.OrderHistory(int customerID) 
        {
            await this.Settings();
            var api = new ApiClientService(ClientSettings, _config);
            var result = await api.OrderHistory(customerID);
            return result;
        }



        #endregion

        #region Private Methods

        // Hit SQL to return OAUTH 2.0 Access tokens
        async private Task Settings() {
            var p = new DynamicParameters();
            string sql = @"SELECT * FROM DKTokens WHERE ID = @ID";
            p.Add("@ID", 1, DbType.Int32);
            using (IDbConnection conn = Connection)
            {
                var settings = await conn.QueryAsync<ApiClientSettings>(sql, p, commandType: CommandType.Text);
                ClientSettings = settings.Single();
            }
        }

        

        #endregion



    }

}
