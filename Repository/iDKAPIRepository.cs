using System.Threading.Tasks;

namespace DKWebAPI
{

    public interface IDKAPIRepository
    {

        // Request a new token from DK API
        Task<ApiClientSettings> GetRefreshToken(string token);

        // Gets the current access tokens
        Task<ApiClientSettings> GetTokens();

        // Invoke DK PartSearch -> KeywordSearch using POST Method
        Task<string> KeywordSearch(KeywordSearchRequest searchTerm);

        // Invoke DK Products -> Search using GET Method
        Task<string> Products(string searchTerm);


        // Invoke DK Products -> Search using GET Method for Quotation System 
        // Option of SQL Storage
        Task<string> Products(string searchTerm, bool writeToSQL);


        // Invoke DK OrderDetails -> Search using GET Method
        Task<string> OrderStatus(int salesOrderID);

        // Invoke DK OrderDetails -> Search using GET Method        
        Task<string> OrderHistory(int customerID);


    }


}